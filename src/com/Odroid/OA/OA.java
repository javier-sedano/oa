package com.Odroid.OA;

import com.admob.android.ads.AdView;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageView.ScaleType;

public class OA extends OABaseActivity {
  public static String INTENT_ROUND = "INTENT_ROUND";

  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.main);
  }

  @Override
  protected void onResume() {
    super.onResume();
    ((AdView) findViewById(R.id.ad_main)).requestFreshAd();
    showState();
  }

  private void restart() {
    ((ImageView) findViewById(R.id.image)).setScaleType(ScaleType.FIT_XY);
    OPreferences.setWord1("");
    OPreferences.setScore1(0);
    OPreferences.setNumber2("");
    OPreferences.setScore2(0);
    OPreferences.setWord3("");
    OPreferences.setScore3(0);
    OPreferences.setNumber4("");
    OPreferences.setScore4(0);
    OPreferences.setWord5("");
    OPreferences.setScore5(0);
    OPreferences.setNumber6("");
    OPreferences.setScore6(0);
    OPreferences.setWord7("");
    OPreferences.setScore7(0);
    OPreferences.setNumber8("");
    OPreferences.setScore8(0);
  }

  private void showState() {
    String word1 = OPreferences.getWord1();
    int score1 = OPreferences.getScore1();
    String number2 = OPreferences.getNumber2();
    int score2 = OPreferences.getScore2();
    String word3 = OPreferences.getWord3();
    int score3 = OPreferences.getScore3();
    String number4 = OPreferences.getNumber4();
    int score4 = OPreferences.getScore4();
    String word5 = OPreferences.getWord5();
    int score5 = OPreferences.getScore5();
    String number6 = OPreferences.getNumber6();
    int score6 = OPreferences.getScore6();
    String word7 = OPreferences.getWord7();
    int score7 = OPreferences.getScore7();
    String number8 = OPreferences.getNumber8();
    int score8 = OPreferences.getScore8();
    int score = score1 + score2 + score3 + score4 + score5 + score6 + score7 + score8;
    int hiscore = OPreferences.getHiScore();
    if (score > hiscore) {
      hiscore = score;
      OPreferences.setHiScore(hiscore);
    }
    ((TextView) findViewById(R.id.word1_word)).setText(word1);
    ((TextView) findViewById(R.id.word1_score)).setText("" + score1);
    ((TextView) findViewById(R.id.number2_number)).setText(number2);
    ((TextView) findViewById(R.id.number2_score)).setText("" + score2);
    ((TextView) findViewById(R.id.word3_word)).setText(word3);
    ((TextView) findViewById(R.id.word3_score)).setText("" + score3);
    ((TextView) findViewById(R.id.number4_number)).setText(number4);
    ((TextView) findViewById(R.id.number4_score)).setText("" + score4);
    ((TextView) findViewById(R.id.word5_word)).setText(word5);
    ((TextView) findViewById(R.id.word5_score)).setText("" + score5);
    ((TextView) findViewById(R.id.number6_number)).setText(number6);
    ((TextView) findViewById(R.id.number6_score)).setText("" + score6);
    ((TextView) findViewById(R.id.word7_word)).setText(word7);
    ((TextView) findViewById(R.id.word7_score)).setText("" + score7);
    ((TextView) findViewById(R.id.number8_number)).setText(number8);
    ((TextView) findViewById(R.id.number8_score)).setText("" + score8);
    ((TextView) findViewById(R.id.total_score)).setText("" + score);
    ((TextView) findViewById(R.id.hiscore_score)).setText("" + hiscore);
    if (word1.equals("") || number2.equals("") || word3.equals("") || number4.equals("") || word5.equals("") || number6.equals("") || word7.equals("")
        || number8.equals("")) {
      ((Button) findViewById(R.id.next_round)).setEnabled(true);
    } else {
      ((Button) findViewById(R.id.next_round)).setEnabled(false);
    }
    if (word1.equals("") && number2.equals("") && word3.equals("") && number4.equals("") && word5.equals("") && number6.equals("") && word7.equals("")
        && number8.equals("")) {
      ((Button) findViewById(R.id.restart)).setEnabled(false);
    } else {
      ((Button) findViewById(R.id.restart)).setEnabled(true);
    }
  }

  public void click_next_round(View v) {
    // RandomLetter rl = new RandomLetter();
    // for (int i = 0; i < 9; i++) {
    // String l = rl.get();
    // }
    // if (1 == 1) return;
    // if (1 == 1) {
    // startWord(1);
    // // startNumber(2);
    // return;
    // }
    if (OPreferences.getWord1().equals("")) {
      startWord(1);
    } else if (OPreferences.getNumber2().equals("")) {
      startNumber(2);
    } else if (OPreferences.getWord3().equals("")) {
      startWord(3);
    } else if (OPreferences.getNumber4().equals("")) {
      startNumber(4);
    } else if (OPreferences.getWord5().equals("")) {
      startWord(5);
    } else if (OPreferences.getNumber6().equals("")) {
      startNumber(6);
    } else if (OPreferences.getWord7().equals("")) {
      startWord(7);
    } else if (OPreferences.getNumber8().equals("")) {
      startNumber(8);
    } else {
      // Fallback in case of bug
      restart();
      showState();
    }
  }

  public void click_restart(View v) {
    restart();
    showState();
    click_next_round(v);
  }

  private void startWord(int round) {
    OPreferences.setRound(round);
    OPreferences.setWSeconds(OAWord.SECONDS_INIT + 1); // Force a start
    Intent intent = new Intent(this, OAWord.class);
    startActivityForResult(intent, 0);
  }

  private void startNumber(int round) {
    OPreferences.setRound(round);
    OPreferences.setNSeconds(OANumber.SECONDS_INIT + 1); // Force a start
    Intent intent = new Intent(this, OANumber.class);
    startActivityForResult(intent, 0);
  }

  @Override
  protected void onActivityResult(int requestCode, int resultCode, Intent data) {
    showState();
  }

}