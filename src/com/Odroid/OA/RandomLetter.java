package com.Odroid.OA;

import java.util.Random;

public class RandomLetter {
  private static float A = 10.3485f;
  private static float B = 1.456f;
  private static float C = 3.731f;
  private static float D = 5.0565f;
  private static float E = 13.191f;
  private static float F = 1.459f;
  private static float G = 1.5125f;
  private static float H = 3.397f;
  private static float I = 6.608f;
  private static float J = 0.2965f;
  private static float K = 0.391f;
  private static float L = 4.4975f;
  private static float M = 2.778f;
  private static float N = 6.7295f;
  private static float O = 8.0935f;
  private static float P = 2.2195f;
  private static float Q = 0.4875f;
  private static float R = 6.4285f;
  private static float S = 7.1535f;
  private static float T = 6.843f;
  private static float U = 3.344f;
  private static float V = 0.939f;
  private static float W = 1.19f;
  private static float X = 0.185f;
  private static float Y = 1.437f;
  private static float Z = 0.297f;

  private static Random random = new Random();

  private int total = 0;
  private int vowel = 0;

  public String get() {
    if (total == 0) { return getSet(); }
    float ratio = 1.0f * vowel / total;
    // android.util.Log.d("0A", "0A: ratio " + ratio + " = " + vowel + "/" +
    // total);
    if (ratio < 0.4) { // Lacks vowels
    // android.util.Log.d("0A", "0A: <0.4");
      String letter = getOne();
      if (letter.equals("A") || letter.equals("E") || letter.equals("I") || letter.equals("O") || letter.equals("U")) {
        total++;
        vowel++;
        // android.util.Log.d("0A", "0A: <0.4   " + letter);
        return letter;
      } else { // If it is not a vowel, seconds chance
      // android.util.Log.d("0A", "0A: <0.4  x" + letter);
        return getSet();
      }
    } else if (ratio > 0.601) { // Lacks consonants (3/5 > 0.6 !!!!)
    // android.util.Log.d("0A", "0A: >0.6");
      String letter = getOne();
      if (letter.equals("A") || letter.equals("E") || letter.equals("I") || letter.equals("O") || letter.equals("U")) {
        // If it is not a consonant, second chance
        // android.util.Log.d("0A", "0A: >0.6  x" + letter);
        return getSet();
      } else {
        total++;
        // android.util.Log.d("0A", "0A: >0.6   " + letter);
        return letter;
      }
    } else {
      return getSet();
    }
  }

  private String getSet() {
    total++;
    String letter = getOne();
    if (letter.equals("A") || letter.equals("E") || letter.equals("I") || letter.equals("O") || letter.equals("U")) {
      vowel++;
    }
    android.util.Log.d("0A", "0A:        " + letter);
    return letter;
  }

  private String getOne() {
    float r = random.nextFloat() * 100;
    if (r < A) return "A";
    r -= A;
    if (r < B) return "B";
    r -= B;
    if (r < C) return "C";
    r -= C;
    if (r < D) return "D";
    r -= D;
    if (r < E) return "E";
    r -= E;
    if (r < F) return "F";
    r -= F;
    if (r < G) return "G";
    r -= G;
    if (r < H) return "H";
    r -= H;
    if (r < I) return "I";
    r -= I;
    if (r < J) return "J";
    r -= J;
    if (r < K) return "K";
    r -= K;
    if (r < L) return "L";
    r -= L;
    if (r < M) return "M";
    r -= M;
    if (r < N) return "N";
    r -= N;
    if (r < O) return "O";
    r -= O;
    if (r < P) return "P";
    r -= P;
    if (r < Q) return "Q";
    r -= Q;
    if (r < R) return "R";
    r -= R;
    if (r < S) return "S";
    r -= S;
    if (r < T) return "T";
    r -= T;
    if (r < U) return "U";
    r -= U;
    if (r < V) return "V";
    r -= V;
    if (r < W) return "W";
    r -= W;
    if (r < X) return "X";
    r -= X;
    if (r < Y) return "Y";
    r -= Y;
    if (r < Z) return "Z";
    r -= Z;
    return "Z"; // Fallback
  }
}
