package com.Odroid.OA;

import java.util.Random;

public class RandomDigit {
  private static float D1 = 7.14f;
  private static float D2 = 7.14f;
  private static float D3 = 7.14f;
  private static float D4 = 7.14f;
  private static float D5 = 7.14f;
  private static float D6 = 7.14f;
  private static float D7 = 7.14f;
  private static float D8 = 7.14f;
  private static float D9 = 7.14f;
  private static float D10 = 7.14f;
  private static float D25 = 7.14f;
  private static float D50 = 7.14f;
  private static float D75 = 7.14f;
  private static float D100 = 7.14f;

  private static Random random = new Random();

  public static int get() {
    float r = random.nextFloat() * 100;
    if (r < D1) return 1;
    r -= D1;
    if (r < D2) return 2;
    r -= D2;
    if (r < D3) return 3;
    r -= D3;
    if (r < D4) return 4;
    r -= D4;
    if (r < D5) return 5;
    r -= D5;
    if (r < D6) return 6;
    r -= D6;
    if (r < D7) return 7;
    r -= D7;
    if (r < D8) return 8;
    r -= D8;
    if (r < D9) return 9;
    r -= D9;
    if (r < D10) return 10;
    r -= D10;
    if (r < D25) return 25;
    r -= D25;
    if (r < D50) return 50;
    r -= D50;
    if (r < D75) return 75;
    r -= D75;
    if (r < D100) return 100;
    r -= D100;
    return 100; // Fallback
  }

  public static int target() {
    int cent = random.nextInt(9) + 1; // Allways >= 100
    int dec = random.nextInt(10);
    int unit = random.nextInt(10);
    int r = 100 * cent + 10 * dec + unit;
    return r;
  }
}
