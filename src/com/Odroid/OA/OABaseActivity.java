package com.Odroid.OA;

import android.R.drawable;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.media.MediaPlayer;
import android.media.MediaPlayer.OnCompletionListener;
import android.net.Uri;
import android.os.Bundle;
import android.os.PowerManager;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;

import com.admob.android.ads.AdManager;

public class OABaseActivity extends Activity {
  private static final int MENU_ABOUT = 1;
  private static final int MENU_WEB = 2;
  private static final int MENU_MARKET = 3;
  private static final int MENU_SETTINGS = 4;

  PowerManager _powerManagement = null;
  PowerManager.WakeLock _wakeLock = null;

  @Override
  public boolean onCreateOptionsMenu(Menu menu) {
    super.onCreateOptionsMenu(menu);
    menu.add(0, MENU_ABOUT, 0, R.string.menu_about).setIcon(R.drawable.menuicon_about);
    menu.add(0, MENU_WEB, 0, R.string.menu_web).setIcon(R.drawable.menuicon_web);
    menu.add(0, MENU_MARKET, 0, R.string.menu_market).setIcon(R.drawable.menuicon_market);
    menu.add(0, MENU_SETTINGS, 0, R.string.menu_settings).setIcon(drawable.ic_menu_preferences);
    return true;
  }

  @Override
  public boolean onOptionsItemSelected(MenuItem item) {
    switch (item.getItemId()) {
    case MENU_ABOUT:
      AlertDialog.Builder builder = new AlertDialog.Builder(this);
      builder.setTitle(R.string.about_title).setMessage(R.string.about_text).setCancelable(false).setIcon(R.drawable.icon)
          .setNeutralButton(R.string.about_button, new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int id) {
              dialog.cancel();
            }
          }).show();
    break;
    case MENU_WEB:
      _web();
    break;
    case MENU_MARKET:
      _market();
    break;
    case MENU_SETTINGS:
      Intent preferencesIntent = new Intent(getApplicationContext(), OPreferences.class);
      startActivity(preferencesIntent);
      return true;
    }
    return false;
  }

  private void _web() {
    String url = getString(R.string.web);
    Intent i = new Intent(Intent.ACTION_VIEW);
    i.setData(Uri.parse(url));
    startActivity(i);
  }

  private void _market() {
    String url = getString(R.string.market);
    Intent i = new Intent(Intent.ACTION_VIEW);
    i.setData(Uri.parse(url));
    try {
      startActivity(i);
    } catch (ActivityNotFoundException e) {
      _web();
    }
  }

  /** Called when the activity is first created. */
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    OPreferences.setSharedPreferences(PreferenceManager.getDefaultSharedPreferences(getApplicationContext()));
    AdManager.setTestDevices(new String[] { AdManager.TEST_EMULATOR, // Android
                                                                     // emulator
    // "92C1B56F7C969AC6A37A2CCBED67F4E6", // My phone
        });
  }

  @Override
  protected void onResume() {
    super.onResume();
    _keepOnStart();
  }

  @Override
  protected void onPause() {
    _keepOnStop();
    super.onPause();
  }

  private void _keepOnStart() {
    if (_powerManagement == null) {
      _powerManagement = (PowerManager) getSystemService(Context.POWER_SERVICE);
    }
    if (_wakeLock == null) {
      _wakeLock = _powerManagement.newWakeLock(PowerManager.SCREEN_BRIGHT_WAKE_LOCK | PowerManager.ON_AFTER_RELEASE, "0 Bubble keep on");
    }
    _wakeLock.acquire();
  }

  private void _keepOnStop() {
    if ((_wakeLock != null) && (_wakeLock.isHeld())) {
      _wakeLock.release();
    }
  }

  protected void playFX(int res) {
    if (OPreferences.getFxVolume() == 0.0) return;
    MediaPlayer m = MediaPlayer.create(this, res);
    if (m == null) return;
    m.setVolume(OPreferences.getFxVolume(), OPreferences.getFxVolume());
    m.setOnCompletionListener(new OnCompletionListener() {
      public void onCompletion(MediaPlayer mp) {
        mp.release();
      }
    });
    m.start();
  }

}
