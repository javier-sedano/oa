package com.Odroid.OA;

import com.admob.android.ads.AdView;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class OANumber extends OABaseActivity {
  public static int SECONDS_INIT = 60; // 60
  public static int SECONDS_END = -10;
  private static String PLUS = "+";
  private static String MINUS = "-";
  private static String TIMES = "*";
  private static String BETWEEN = "/";

  private Handler uiHandler = new Handler();
  private Runnable tickRunnable = new Runnable() {
    public void run() {
      tick();
    }
  };

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.number);
  }

  @Override
  protected void onResume() {
    super.onResume();
    ((AdView) findViewById(R.id.ad_numbers)).requestFreshAd();
    showState();
  }

  @Override
  protected void onPause() {
    uiHandler.removeCallbacks(tickRunnable);
    super.onPause();
  }

  @Override
  public void onBackPressed () {
    OPreferences.setNScore(0);
    OPreferences.setNSeconds(SECONDS_END);
    setGlobals();
    finish();
  }
  
  private void showState() {
    int seconds = OPreferences.getNSeconds();
    if (seconds > SECONDS_INIT) {
      showStateInit();
    } else if (seconds > 0) {
      showStateClock();
    } else if (seconds > SECONDS_END) {
      showStateChecking();
    } else {
      showStateNext();
    }
  }

  private void showStateInit() {
    OPreferences.setDigit1(RandomDigit.get());
    ((Button) findViewById(R.id.numbers_digit1)).setText(OPreferences.get3Digit1());
    ((Button) findViewById(R.id.numbers_digit1)).setEnabled(true);
    OPreferences.setDigit2(RandomDigit.get());
    ((Button) findViewById(R.id.numbers_digit2)).setText(OPreferences.get3Digit2());
    ((Button) findViewById(R.id.numbers_digit2)).setEnabled(true);
    OPreferences.setDigit3(RandomDigit.get());
    ((Button) findViewById(R.id.numbers_digit3)).setText(OPreferences.get3Digit3());
    ((Button) findViewById(R.id.numbers_digit3)).setEnabled(true);
    OPreferences.setDigit4(RandomDigit.get());
    ((Button) findViewById(R.id.numbers_digit4)).setText(OPreferences.get3Digit4());
    ((Button) findViewById(R.id.numbers_digit4)).setEnabled(true);
    OPreferences.setDigit5(RandomDigit.get());
    ((Button) findViewById(R.id.numbers_digit5)).setText(OPreferences.get3Digit5());
    ((Button) findViewById(R.id.numbers_digit5)).setEnabled(true);
    OPreferences.setDigit6(RandomDigit.get());
    ((Button) findViewById(R.id.numbers_digit6)).setText(OPreferences.get3Digit6());
    ((Button) findViewById(R.id.numbers_digit6)).setEnabled(true);

    ((Button) findViewById(R.id.numbers_plus)).setEnabled(true);
    ((Button) findViewById(R.id.numbers_minus)).setEnabled(true);
    ((Button) findViewById(R.id.numbers_times)).setEnabled(true);
    ((Button) findViewById(R.id.numbers_between)).setEnabled(true);

    OPreferences.setNumber(0);
    OPreferences.setTarget(RandomDigit.target());
    OPreferences.setNSeconds(SECONDS_INIT);
    OPreferences.setNScore(0);
    OPreferences.setCalc("");

    ((TextView) findViewById(R.id.numbers_target)).setText(OPreferences.getNumber() + " / " + OPreferences.getTarget());
    ((TextView) findViewById(R.id.numbers_clock)).setText("");
    ((TextView) findViewById(R.id.numbers_calc)).setText("");
    ((Button) findViewById(R.id.numbers_check)).setEnabled(false);
    ((TextView) findViewById(R.id.numbers_score_score)).setText("");
    ((Button) findViewById(R.id.numbers_next)).setEnabled(false);

    OPreferences.setOp("");
    OPreferences.setOp1(0);
    OPreferences.setOp2(0);

    showState();
  }

  private void showStateClock() {
    if (!calc()) {
      playFX(R.raw.bang);
      OPreferences.setNumber(0);
      check();
      return;
    }

    ((Button) findViewById(R.id.numbers_digit1)).setText(OPreferences.get3Digit1());
    ((Button) findViewById(R.id.numbers_digit1)).setEnabled(OPreferences.getDigit1() > 0);
    ((Button) findViewById(R.id.numbers_digit2)).setText(OPreferences.get3Digit2());
    ((Button) findViewById(R.id.numbers_digit2)).setEnabled(OPreferences.getDigit2() > 0);
    ((Button) findViewById(R.id.numbers_digit3)).setText(OPreferences.get3Digit3());
    ((Button) findViewById(R.id.numbers_digit3)).setEnabled(OPreferences.getDigit3() > 0);
    ((Button) findViewById(R.id.numbers_digit4)).setText(OPreferences.get3Digit4());
    ((Button) findViewById(R.id.numbers_digit4)).setEnabled(OPreferences.getDigit4() > 0);
    ((Button) findViewById(R.id.numbers_digit5)).setText(OPreferences.get3Digit5());
    ((Button) findViewById(R.id.numbers_digit5)).setEnabled(OPreferences.getDigit5() > 0);
    ((Button) findViewById(R.id.numbers_digit6)).setText(OPreferences.get3Digit6());
    ((Button) findViewById(R.id.numbers_digit6)).setEnabled(OPreferences.getDigit6() > 0);

    ((Button) findViewById(R.id.numbers_plus)).setEnabled(true);
    ((Button) findViewById(R.id.numbers_minus)).setEnabled(true);
    ((Button) findViewById(R.id.numbers_times)).setEnabled(true);
    ((Button) findViewById(R.id.numbers_between)).setEnabled(true);

    ((TextView) findViewById(R.id.numbers_target)).setText(OPreferences.getNumber() + " / " + OPreferences.getTarget());
    ((TextView) findViewById(R.id.numbers_clock)).setText("" + OPreferences.getNSeconds());
    ((TextView) findViewById(R.id.numbers_calc)).setText(OPreferences.getCalc());
    ((Button) findViewById(R.id.numbers_check)).setEnabled(true);
    ((TextView) findViewById(R.id.numbers_score_score)).setText("");
    ((Button) findViewById(R.id.numbers_next)).setEnabled(false);

    uiHandler.removeCallbacks(tickRunnable);
    uiHandler.postDelayed(tickRunnable, 1000);
  }

  /**
   * Returns false on a user mistake that must abort the calculation
   */
  private boolean calc() {
    int op1 = OPreferences.getOp1();
    int op2 = OPreferences.getOp2();
    String op = OPreferences.getOp();
    // Fill calc, if needed
    if (op1 != 0) {
      String calc = "";
      if (op1 != 0) calc += op1;
      if (!op.equals("")) calc += " " + op;
      if (op2 != 0) calc += " " + op2;
      OPreferences.setCalc(calc);
    }
    // Reorder numbers, if needed
    if (op2 != 0) {
      int res = 0;
      if (op.equals(PLUS)) {
        res = op1 + op2;
      } else if (op.equals(MINUS)) {
        res = op1 - op2;
        if (res <= 0) { return false; }
      } else if (op.equals(TIMES)) {
        res = op1 * op2;
      } else if (op.equals(BETWEEN)) {
        res = op1 / op2;
        if (res * op2 != op1) { return false; }
      } else {
        res = op1 + op2;
      }
      OPreferences.setCalc(OPreferences.getCalc() + " = " + res);
      // In this moment, only the two ops are < 0
      if (OPreferences.getDigit1() < 0) OPreferences.setDigit1(0);
      if (OPreferences.getDigit2() < 0) OPreferences.setDigit2(0);
      if (OPreferences.getDigit3() < 0) OPreferences.setDigit3(0);
      if (OPreferences.getDigit4() < 0) OPreferences.setDigit4(0);
      if (OPreferences.getDigit5() < 0) OPreferences.setDigit5(0);
      if (OPreferences.getDigit6() < 0) OPreferences.setDigit6(0);
      // Now, put the new number in a cell
      if (OPreferences.getDigit1() == 0) OPreferences.setDigit1(res);
      else if (OPreferences.getDigit2() == 0) OPreferences.setDigit2(res);
      else if (OPreferences.getDigit3() == 0) OPreferences.setDigit3(res);
      else if (OPreferences.getDigit4() == 0) OPreferences.setDigit4(res);
      else if (OPreferences.getDigit5() == 0) OPreferences.setDigit5(res);
      else if (OPreferences.getDigit6() == 0) OPreferences.setDigit6(res);
      else { // BUG
      }
      OPreferences.setOp1(0);
      OPreferences.setOp2(0);
      OPreferences.setOp("");
      int number = OPreferences.getNumber();
      int target = OPreferences.getTarget();
      int diffnew = Math.abs(res - target);
      int diffold = Math.abs(number - target);
      if (diffnew < diffold) {
        OPreferences.setNumber(res);
      }
    }
    return true;
  }

  private void tick() {
    int nseconds = OPreferences.getNSeconds();
    nseconds--;
    OPreferences.setNSeconds(nseconds);
    playFX(R.raw.tick);
    showState();
  }

  private void showStateChecking() {
    if (!calc()) {
      playFX(R.raw.bang);
      OPreferences.setNumber(0);
      check();
      return;
    }

    ((Button) findViewById(R.id.numbers_digit1)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_digit2)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_digit3)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_digit4)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_digit5)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_digit6)).setEnabled(false);

    ((Button) findViewById(R.id.numbers_plus)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_minus)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_times)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_between)).setEnabled(false);

    ((TextView) findViewById(R.id.numbers_target)).setText(OPreferences.getNumber() + " / " + OPreferences.getTarget());
    ((TextView) findViewById(R.id.numbers_clock)).setText("");
    ((TextView) findViewById(R.id.numbers_calc)).setText(OPreferences.getCalc());
    ((Button) findViewById(R.id.numbers_check)).setEnabled(false);
    ((TextView) findViewById(R.id.numbers_score_score)).setText("");
    ((Button) findViewById(R.id.numbers_next)).setEnabled(false);

    check();
  }

  private void check() {
    int number = OPreferences.getNumber();
    int target = OPreferences.getTarget();
    int diff = Math.abs(number - target);
    int score = 0;
    if (diff == 0) {
      score = 9;
      playFX(R.raw.aplause);
    } else if (diff < 100) {
      score = (int) Math.round(5 / Math.log10(5 * diff) - 1);
      playFX(R.raw.smallaplause);
    } else {
      score = 0;
    }
    OPreferences.setNScore(score);
    OPreferences.setNSeconds(SECONDS_END);
    showState();
  }

  private void showStateNext() {
    ((Button) findViewById(R.id.numbers_digit1)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_digit2)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_digit3)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_digit4)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_digit5)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_digit6)).setEnabled(false);

    ((Button) findViewById(R.id.numbers_plus)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_minus)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_times)).setEnabled(false);
    ((Button) findViewById(R.id.numbers_between)).setEnabled(false);

    ((TextView) findViewById(R.id.numbers_target)).setText(OPreferences.getNumber() + " / " + OPreferences.getTarget());
    ((TextView) findViewById(R.id.numbers_clock)).setText("");
    ((TextView) findViewById(R.id.numbers_calc)).setText(OPreferences.getCalc());
    ((Button) findViewById(R.id.numbers_check)).setEnabled(false);
    ((TextView) findViewById(R.id.numbers_score_score)).setText("" + OPreferences.getNScore());
    ((Button) findViewById(R.id.numbers_next)).setEnabled(true);

    setGlobals();
  }

  private void setGlobals() {
    String number = OPreferences.getNumber() + " / " + OPreferences.getTarget();
    int round = OPreferences.getRound();
    if (round == 2) {
      OPreferences.setNumber2(number);
      OPreferences.setScore2(OPreferences.getNScore());
    } else if (round == 4) {
      OPreferences.setNumber4(number);
      OPreferences.setScore4(OPreferences.getNScore());
    } else if (round == 6) {
      OPreferences.setNumber6(number);
      OPreferences.setScore6(OPreferences.getNScore());
    } else if (round == 8) {
      OPreferences.setNumber8(number);
      OPreferences.setScore8(OPreferences.getNScore());
    } else {
      return;
    }
  }

  public void click_check(View v) {
    OPreferences.setNSeconds(0);
    showState();
  }

  public void click_next(View v) {
    finish();
  }

  public void click_plus(View v) {
    playFX(R.raw.click);
    if (OPreferences.getOp1() <= 0) return;
    OPreferences.setOp(PLUS);
    showState();
  }

  public void click_minus(View v) {
    playFX(R.raw.click);
    if (OPreferences.getOp1() <= 0) return;
    OPreferences.setOp(MINUS);
    showState();
  }

  public void click_times(View v) {
    playFX(R.raw.click);
    if (OPreferences.getOp1() <= 0) return;
    OPreferences.setOp(TIMES);
    showState();
  }

  public void click_between(View v) {
    playFX(R.raw.click);
    if (OPreferences.getOp1() <= 0) return;
    OPreferences.setOp(BETWEEN);
    showState();
  }

  public void reenableop1() {
    if (OPreferences.getDigit1() < 0) OPreferences.setDigit1(-OPreferences.getDigit1());
    else if (OPreferences.getDigit2() < 0) OPreferences.setDigit2(-OPreferences.getDigit2());
    else if (OPreferences.getDigit3() < 0) OPreferences.setDigit3(-OPreferences.getDigit3());
    else if (OPreferences.getDigit4() < 0) OPreferences.setDigit4(-OPreferences.getDigit4());
    else if (OPreferences.getDigit5() < 0) OPreferences.setDigit5(-OPreferences.getDigit5());
    else if (OPreferences.getDigit6() < 0) OPreferences.setDigit6(-OPreferences.getDigit6());
  }

  public void click_digit1(View v) {
    playFX(R.raw.click);
    if (!OPreferences.getOp().equals("")) {
      OPreferences.setOp2(OPreferences.getDigit1());
      OPreferences.setDigit1(-OPreferences.getDigit1());
      showState();
      return;
    }
    if (OPreferences.getOp1() > 0) {
      reenableop1();
    }
    OPreferences.setOp1(OPreferences.getDigit1());
    OPreferences.setDigit1(-OPreferences.getDigit1());
    showState();
  }

  public void click_digit2(View v) {
    playFX(R.raw.click);
    if (!OPreferences.getOp().equals("")) {
      OPreferences.setOp2(OPreferences.getDigit2());
      OPreferences.setDigit2(-OPreferences.getDigit2());
      showState();
      return;
    }
    if (OPreferences.getOp1() > 0) {
      reenableop1();
    }
    OPreferences.setOp1(OPreferences.getDigit2());
    OPreferences.setDigit2(-OPreferences.getDigit2());
    showState();
  }

  public void click_digit3(View v) {
    playFX(R.raw.click);
    if (!OPreferences.getOp().equals("")) {
      OPreferences.setOp2(OPreferences.getDigit3());
      OPreferences.setDigit3(-OPreferences.getDigit3());
      showState();
      return;
    }
    if (OPreferences.getOp1() > 0) {
      reenableop1();
    }
    OPreferences.setOp1(OPreferences.getDigit3());
    OPreferences.setDigit3(-OPreferences.getDigit3());
    showState();
  }

  public void click_digit4(View v) {
    playFX(R.raw.click);
    if (!OPreferences.getOp().equals("")) {
      OPreferences.setOp2(OPreferences.getDigit4());
      OPreferences.setDigit4(-OPreferences.getDigit4());
      showState();
      return;
    }
    if (OPreferences.getOp1() > 0) {
      reenableop1();
    }
    OPreferences.setOp1(OPreferences.getDigit4());
    OPreferences.setDigit4(-OPreferences.getDigit4());
    showState();
  }

  public void click_digit5(View v) {
    playFX(R.raw.click);
    if (!OPreferences.getOp().equals("")) {
      OPreferences.setOp2(OPreferences.getDigit5());
      OPreferences.setDigit5(-OPreferences.getDigit5());
      showState();
      return;
    }
    if (OPreferences.getOp1() > 0) {
      reenableop1();
    }
    OPreferences.setOp1(OPreferences.getDigit5());
    OPreferences.setDigit5(-OPreferences.getDigit5());
    showState();
  }

  public void click_digit6(View v) {
    playFX(R.raw.click);
    if (!OPreferences.getOp().equals("")) {
      OPreferences.setOp2(OPreferences.getDigit6());
      OPreferences.setDigit6(-OPreferences.getDigit6());
      showState();
      return;
    }
    if (OPreferences.getOp1() > 0) {
      reenableop1();
    }
    OPreferences.setOp1(OPreferences.getDigit6());
    OPreferences.setDigit6(-OPreferences.getDigit6());
    showState();
  }

}