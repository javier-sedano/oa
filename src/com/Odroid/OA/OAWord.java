package com.Odroid.OA;

import com.admob.android.ads.AdView;

import android.os.Bundle;
import android.os.Handler;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

public class OAWord extends OABaseActivity {
  public static int SECONDS_INIT = 60; // 60
  public static int SECONDS_END = -10;

  private Handler uiHandler = new Handler();
  private Runnable tickRunnable = new Runnable() {
    public void run() {
      tick();
    }
  };

  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.word);
  }

  @Override
  protected void onResume() {
    super.onResume();
    ((AdView) findViewById(R.id.ad_words)).requestFreshAd();
    showState();
  }

  @Override
  protected void onPause() {
    uiHandler.removeCallbacks(tickRunnable);
    super.onPause();
  }

  @Override
  public void onBackPressed() {
    OPreferences.setWScore(0);
    OPreferences.setWSeconds(SECONDS_END);
    setGlobals();
    finish();
  }

  private void showState() {
    int seconds = OPreferences.getWSeconds();
    if (seconds > SECONDS_INIT) {
      showStateInit();
    } else if (seconds > 0) {
      showStateClock();
    } else if (seconds > SECONDS_END) {
      showStateChecking();
    } else {
      showStateNext();
    }
  }

  private void showStateInit() {
    RandomLetter rl = new RandomLetter();
    OPreferences.setLetter1(rl.get());
    ((Button) findViewById(R.id.words_letter1)).setText(OPreferences.getLetter1());
    ((Button) findViewById(R.id.words_letter1)).setEnabled(true);
    OPreferences.setLetter2(rl.get());
    ((Button) findViewById(R.id.words_letter2)).setText(OPreferences.getLetter2());
    ((Button) findViewById(R.id.words_letter2)).setEnabled(true);
    OPreferences.setLetter3(rl.get());
    ((Button) findViewById(R.id.words_letter3)).setText(OPreferences.getLetter3());
    ((Button) findViewById(R.id.words_letter3)).setEnabled(true);
    OPreferences.setLetter4(rl.get());
    ((Button) findViewById(R.id.words_letter4)).setText(OPreferences.getLetter4());
    ((Button) findViewById(R.id.words_letter4)).setEnabled(true);
    OPreferences.setLetter5(rl.get());
    ((Button) findViewById(R.id.words_letter5)).setText(OPreferences.getLetter5());
    ((Button) findViewById(R.id.words_letter5)).setEnabled(true);
    OPreferences.setLetter6(rl.get());
    ((Button) findViewById(R.id.words_letter6)).setText(OPreferences.getLetter6());
    ((Button) findViewById(R.id.words_letter6)).setEnabled(true);
    OPreferences.setLetter7(rl.get());
    ((Button) findViewById(R.id.words_letter7)).setText(OPreferences.getLetter7());
    ((Button) findViewById(R.id.words_letter7)).setEnabled(true);
    OPreferences.setLetter8(rl.get());
    ((Button) findViewById(R.id.words_letter8)).setText(OPreferences.getLetter8());
    ((Button) findViewById(R.id.words_letter8)).setEnabled(true);
    OPreferences.setLetter9(rl.get());
    ((Button) findViewById(R.id.words_letter9)).setText(OPreferences.getLetter9());
    ((Button) findViewById(R.id.words_letter9)).setEnabled(true);

    OPreferences.setWSeconds(SECONDS_INIT);
    OPreferences.setWord("");
    OPreferences.setWScore(0);

    ((TextView) findViewById(R.id.words_clock)).setText("");
    ((TextView) findViewById(R.id.words_word)).setText("");
    ((Button) findViewById(R.id.words_check)).setEnabled(false);
    ((TextView) findViewById(R.id.words_score_score)).setText("");
    ((Button) findViewById(R.id.words_next)).setEnabled(false);

    showState();
  }

  private void showStateClock() {
    ((Button) findViewById(R.id.words_letter1)).setText(OPreferences.getLetter1().replace("-", ""));
    ((Button) findViewById(R.id.words_letter1)).setEnabled(!OPreferences.getLetter1().startsWith("-"));
    ((Button) findViewById(R.id.words_letter2)).setText(OPreferences.getLetter2().replace("-", ""));
    ((Button) findViewById(R.id.words_letter2)).setEnabled(!OPreferences.getLetter2().startsWith("-"));
    ((Button) findViewById(R.id.words_letter3)).setText(OPreferences.getLetter3().replace("-", ""));
    ((Button) findViewById(R.id.words_letter3)).setEnabled(!OPreferences.getLetter3().startsWith("-"));
    ((Button) findViewById(R.id.words_letter4)).setText(OPreferences.getLetter4().replace("-", ""));
    ((Button) findViewById(R.id.words_letter4)).setEnabled(!OPreferences.getLetter4().startsWith("-"));
    ((Button) findViewById(R.id.words_letter5)).setText(OPreferences.getLetter5().replace("-", ""));
    ((Button) findViewById(R.id.words_letter5)).setEnabled(!OPreferences.getLetter5().startsWith("-"));
    ((Button) findViewById(R.id.words_letter6)).setText(OPreferences.getLetter6().replace("-", ""));
    ((Button) findViewById(R.id.words_letter6)).setEnabled(!OPreferences.getLetter6().startsWith("-"));
    ((Button) findViewById(R.id.words_letter7)).setText(OPreferences.getLetter7().replace("-", ""));
    ((Button) findViewById(R.id.words_letter7)).setEnabled(!OPreferences.getLetter7().startsWith("-"));
    ((Button) findViewById(R.id.words_letter8)).setText(OPreferences.getLetter8().replace("-", ""));
    ((Button) findViewById(R.id.words_letter8)).setEnabled(!OPreferences.getLetter8().startsWith("-"));
    ((Button) findViewById(R.id.words_letter9)).setText(OPreferences.getLetter9().replace("-", ""));
    ((Button) findViewById(R.id.words_letter9)).setEnabled(!OPreferences.getLetter9().startsWith("-"));

    ((TextView) findViewById(R.id.words_clock)).setText("" + OPreferences.getWSeconds());
    ((TextView) findViewById(R.id.words_word)).setText(OPreferences.getWord());
    ((Button) findViewById(R.id.words_check)).setEnabled(true);
    ((TextView) findViewById(R.id.words_score_score)).setText("");
    ((Button) findViewById(R.id.words_next)).setEnabled(false);

    uiHandler.removeCallbacks(tickRunnable);
    uiHandler.postDelayed(tickRunnable, 1000);
  }

  private void tick() {
    int wseconds = OPreferences.getWSeconds();
    wseconds--;
    OPreferences.setWSeconds(wseconds);
    playFX(R.raw.tick);
    showState();
  }

  private void showStateChecking() {
    ((Button) findViewById(R.id.words_letter1)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter2)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter3)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter4)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter5)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter6)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter7)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter8)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter9)).setEnabled(false);

    ((TextView) findViewById(R.id.words_clock)).setText("");
    ((TextView) findViewById(R.id.words_word)).setText(OPreferences.getWord());
    ((Button) findViewById(R.id.words_check)).setEnabled(false);
    ((TextView) findViewById(R.id.words_score_score)).setText("");
    ((Button) findViewById(R.id.words_next)).setEnabled(false);

    check();
  }

  private void check() {
    CharSequence word = OPreferences.getWord();
    int score = word.length();
    if (score == 9) {
      playFX(R.raw.aplause);
      score = 18;
    } else if (score > 0) {
      playFX(R.raw.smallaplause);
    }
    OPreferences.setWScore(score);
    OPreferences.setWSeconds(SECONDS_END);
    showState();
  }

  private void showStateNext() {
    ((Button) findViewById(R.id.words_letter1)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter2)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter3)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter4)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter5)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter6)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter7)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter8)).setEnabled(false);
    ((Button) findViewById(R.id.words_letter9)).setEnabled(false);

    ((TextView) findViewById(R.id.words_clock)).setText("");
    ((TextView) findViewById(R.id.words_word)).setText(OPreferences.getWord());
    ((Button) findViewById(R.id.words_check)).setEnabled(false);
    ((TextView) findViewById(R.id.words_score_score)).setText("" + OPreferences.getWScore());
    ((Button) findViewById(R.id.words_next)).setEnabled(true);

    setGlobals();
  }

  private void setGlobals() {
    String word = "" + ((TextView) findViewById(R.id.words_word)).getText();
    if (word.equals("")) word = "-";
    int round = OPreferences.getRound();
    if (round == 1) {
      OPreferences.setWord1(word);
      OPreferences.setScore1(OPreferences.getWScore());
    } else if (round == 3) {
      OPreferences.setWord3(word);
      OPreferences.setScore3(OPreferences.getWScore());
    } else if (round == 5) {
      OPreferences.setWord5(word);
      OPreferences.setScore5(OPreferences.getWScore());
    } else if (round == 7) {
      OPreferences.setWord7(word);
      OPreferences.setScore7(OPreferences.getWScore());
    } else {
      return;
    }
  }

  public void click_check(View v) {
    OPreferences.setWSeconds(0);
    showState();
  }

  public void click_next(View v) {
    finish();
  }

  public void click_letter1(View v) {
    playFX(R.raw.click);
    String word = ((TextView) findViewById(R.id.words_word)).getText() + OPreferences.getLetter1();
    OPreferences.setWord(word);
    OPreferences.setLetter1("-" + OPreferences.getLetter1());
    showState();
  }

  public void click_letter2(View v) {
    playFX(R.raw.click);
    String word = ((TextView) findViewById(R.id.words_word)).getText() + OPreferences.getLetter2();
    OPreferences.setWord(word);
    OPreferences.setLetter2("-" + OPreferences.getLetter2());
    showState();
  }

  public void click_letter3(View v) {
    playFX(R.raw.click);
    String word = ((TextView) findViewById(R.id.words_word)).getText() + OPreferences.getLetter3();
    OPreferences.setWord(word);
    OPreferences.setLetter3("-" + OPreferences.getLetter3());
    showState();
  }

  public void click_letter4(View v) {
    playFX(R.raw.click);
    String word = ((TextView) findViewById(R.id.words_word)).getText() + OPreferences.getLetter4();
    OPreferences.setWord(word);
    OPreferences.setLetter4("-" + OPreferences.getLetter4());
    showState();
  }

  public void click_letter5(View v) {
    playFX(R.raw.click);
    String word = ((TextView) findViewById(R.id.words_word)).getText() + OPreferences.getLetter5();
    OPreferences.setWord(word);
    OPreferences.setLetter5("-" + OPreferences.getLetter5());
    showState();
  }

  public void click_letter6(View v) {
    playFX(R.raw.click);
    String word = ((TextView) findViewById(R.id.words_word)).getText() + OPreferences.getLetter6();
    OPreferences.setWord(word);
    OPreferences.setLetter6("-" + OPreferences.getLetter6());
    showState();
  }

  public void click_letter7(View v) {
    playFX(R.raw.click);
    String word = ((TextView) findViewById(R.id.words_word)).getText() + OPreferences.getLetter7();
    OPreferences.setWord(word);
    OPreferences.setLetter7("-" + OPreferences.getLetter7());
    showState();
  }

  public void click_letter8(View v) {
    playFX(R.raw.click);
    String word = ((TextView) findViewById(R.id.words_word)).getText() + OPreferences.getLetter8();
    OPreferences.setWord(word);
    OPreferences.setLetter8("-" + OPreferences.getLetter8());
    showState();
  }

  public void click_letter9(View v) {
    playFX(R.raw.click);
    String word = ((TextView) findViewById(R.id.words_word)).getText() + OPreferences.getLetter9();
    OPreferences.setWord(word);
    OPreferences.setLetter9("-" + OPreferences.getLetter9());
    showState();
  }

}