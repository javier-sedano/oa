package com.Odroid.OA;

import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceActivity;

/**
 * Keeps the state of the game and the user configurable preferences
 * 
 */

public class OPreferences extends PreferenceActivity {

  private static SharedPreferences _prefs;

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    // Load the preferences from an XML
    addPreferencesFromResource(R.layout.preferences);
  }

  public static void setSharedPreferences(SharedPreferences s) {
    _prefs = s;
  }

  private static String FXVOLUME = "fxVolume";

  public static float getFxVolume() {
    if (_prefs == null) return 1.0f;
    String vol = _prefs.getString(FXVOLUME, "1.0");
    Float f = new Float(vol);
    return f.floatValue();
  }

  /* General gets and sets */

  public static void setString(String key, String value) {
    if (_prefs == null) return;
    SharedPreferences.Editor e = _prefs.edit();
    e.putString(key, value);
    e.commit();
  }

  public static String getString(String key, String deflt) {
    if (_prefs == null) return deflt;
    return _prefs.getString(key, deflt);
  }

  public static void setInt(String key, int value) {
    if (_prefs == null) return;
    SharedPreferences.Editor e = _prefs.edit();
    e.putInt(key, value);
    e.commit();
  }

  public static int getInt(String key, int deflt) {
    if (_prefs == null) return deflt;
    return _prefs.getInt(key, deflt);
  }

  public static String get3Int(String key) {
    int i = getInt(key, 0);
    if (i < 0) i = -i;
    if (i == 0) return "   ";
    if (i < 10) return "  " + i;
    if (i < 100) return " " + i;
    if (i < 1000) return "" + i;
    return "   ";
  }

  /* Main State */

  private static String WORD1 = "WORD1";

  public static void setWord1(String v) {
    setString(WORD1, v);
  }

  public static String getWord1() {
    return getString(WORD1, "");
  }

  private static String SCORE1 = "SCORE1";

  public static void setScore1(int v) {
    setInt(SCORE1, v);
  }

  public static int getScore1() {
    return getInt(SCORE1, 0);
  }

  private static String NUMBER2 = "NUMBER2";

  public static void setNumber2(String v) {
    setString(NUMBER2, v);
  }

  public static String getNumber2() {
    return getString(NUMBER2, "");
  }

  private static String SCORE2 = "SCORE2";

  public static void setScore2(int v) {
    setInt(SCORE2, v);
  }

  public static int getScore2() {
    return getInt(SCORE2, 0);
  }

  private static String WORD3 = "WORD3";

  public static void setWord3(String v) {
    setString(WORD3, v);
  }

  public static String getWord3() {
    return getString(WORD3, "");
  }

  private static String SCORE3 = "SCORE3";

  public static void setScore3(int v) {
    setInt(SCORE3, v);
  }

  public static int getScore3() {
    return getInt(SCORE3, 0);
  }

  private static String NUMBER4 = "NUMBER4";

  public static void setNumber4(String v) {
    setString(NUMBER4, v);
  }

  public static String getNumber4() {
    return getString(NUMBER4, "");
  }

  private static String SCORE4 = "SCORE4";

  public static void setScore4(int v) {
    setInt(SCORE4, v);
  }

  public static int getScore4() {
    return getInt(SCORE4, 0);
  }

  private static String WORD5 = "WORD5";

  public static void setWord5(String v) {
    setString(WORD5, v);
  }

  public static String getWord5() {
    return getString(WORD5, "");
  }

  private static String SCORE5 = "SCORE5";

  public static void setScore5(int v) {
    setInt(SCORE5, v);
  }

  public static int getScore5() {
    return getInt(SCORE5, 0);
  }

  private static String NUMBER6 = "NUMBER6";

  public static void setNumber6(String v) {
    setString(NUMBER6, v);
  }

  public static String getNumber6() {
    return getString(NUMBER6, "");
  }

  private static String SCORE6 = "SCORE6";

  public static void setScore6(int v) {
    setInt(SCORE6, v);
  }

  public static int getScore6() {
    return getInt(SCORE6, 0);
  }

  private static String WORD7 = "WORD7";

  public static void setWord7(String v) {
    setString(WORD7, v);
  }

  public static String getWord7() {
    return getString(WORD7, "");
  }

  private static String SCORE7 = "SCORE7";

  public static void setScore7(int v) {
    setInt(SCORE7, v);
  }

  public static int getScore7() {
    return getInt(SCORE7, 0);
  }

  private static String NUMBER8 = "NUMBER8";

  public static void setNumber8(String v) {
    setString(NUMBER8, v);
  }

  public static String getNumber8() {
    return getString(NUMBER8, "");
  }

  private static String SCORE8 = "SCORE8";

  public static void setScore8(int v) {
    setInt(SCORE8, v);
  }

  public static int getScore8() {
    return getInt(SCORE8, 0);
  }

  private static String HISCORE = "HISCORE";

  public static void setHiScore(int v) {
    setInt(HISCORE, v);
  }

  public static int getHiScore() {
    return getInt(HISCORE, 0);
  }

  private static String ROUND = "ROUND";

  public static void setRound(int v) {
    setInt(ROUND, v);
  }

  public static int getRound() {
    return getInt(ROUND, 0);
  }

  /* Word round state */

  private static String LETTER1 = "LETTER1";

  public static void setLetter1(String v) {
    setString(LETTER1, v);
  }

  public static String getLetter1() {
    return getString(LETTER1, "");
  }

  private static String LETTER2 = "LETTER2";

  public static void setLetter2(String v) {
    setString(LETTER2, v);
  }

  public static String getLetter2() {
    return getString(LETTER2, "");
  }

  private static String LETTER3 = "LETTER3";

  public static void setLetter3(String v) {
    setString(LETTER3, v);
  }

  public static String getLetter3() {
    return getString(LETTER3, "");
  }

  private static String LETTER4 = "LETTER4";

  public static void setLetter4(String v) {
    setString(LETTER4, v);
  }

  public static String getLetter4() {
    return getString(LETTER4, "");
  }

  private static String LETTER5 = "LETTER5";

  public static void setLetter5(String v) {
    setString(LETTER5, v);
  }

  public static String getLetter5() {
    return getString(LETTER5, "");
  }

  private static String LETTER6 = "LETTER6";

  public static void setLetter6(String v) {
    setString(LETTER6, v);
  }

  public static String getLetter6() {
    return getString(LETTER6, "");
  }

  private static String LETTER7 = "LETTER7";

  public static void setLetter7(String v) {
    setString(LETTER7, v);
  }

  public static String getLetter7() {
    return getString(LETTER7, "");
  }

  private static String LETTER8 = "LETTER8";

  public static void setLetter8(String v) {
    setString(LETTER8, v);
  }

  public static String getLetter8() {
    return getString(LETTER8, "");
  }

  private static String LETTER9 = "LETTER9";

  public static void setLetter9(String v) {
    setString(LETTER9, v);
  }

  public static String getLetter9() {
    return getString(LETTER9, "");
  }

  private static String WSECONDS = "WSECONDS";

  public static void setWSeconds(int v) {
    setInt(WSECONDS, v);
  }

  public static int getWSeconds() {
    return getInt(WSECONDS, 0);
  }

  private static String WORD = "WORD";

  public static void setWord(String v) {
    setString(WORD, v);
  }

  public static String getWord() {
    return getString(WORD, "");
  }

  private static String WSCORE = "WSCORE";

  public static void setWScore(int v) {
    setInt(WSCORE, v);
  }

  public static int getWScore() {
    return getInt(WSCORE, 0);
  }

  /* Number round state */

  private static String DIGIT1 = "DIGIT1";

  public static void setDigit1(int v) {
    setInt(DIGIT1, v);
  }

  public static int getDigit1() {
    return getInt(DIGIT1, 0);
  }

  public static String get3Digit1() {
    return get3Int(DIGIT1);
  }

  private static String DIGIT2 = "DIGIT2";

  public static void setDigit2(int v) {
    setInt(DIGIT2, v);
  }

  public static int getDigit2() {
    return getInt(DIGIT2, 0);
  }

  public static String get3Digit2() {
    return get3Int(DIGIT2);
  }

  private static String DIGIT3 = "DIGIT3";

  public static void setDigit3(int v) {
    setInt(DIGIT3, v);
  }

  public static int getDigit3() {
    return getInt(DIGIT3, 0);
  }

  public static String get3Digit3() {
    return get3Int(DIGIT3);
  }

  private static String DIGIT4 = "DIGIT4";

  public static void setDigit4(int v) {
    setInt(DIGIT4, v);
  }

  public static int getDigit4() {
    return getInt(DIGIT4, 0);
  }

  public static String get3Digit4() {
    return get3Int(DIGIT4);
  }

  private static String DIGIT5 = "DIGIT5";

  public static void setDigit5(int v) {
    setInt(DIGIT5, v);
  }

  public static int getDigit5() {
    return getInt(DIGIT5, 0);
  }

  public static String get3Digit5() {
    return get3Int(DIGIT5);
  }

  private static String DIGIT6 = "DIGIT6";

  public static void setDigit6(int v) {
    setInt(DIGIT6, v);
  }

  public static int getDigit6() {
    return getInt(DIGIT6, 0);
  }

  public static String get3Digit6() {
    return get3Int(DIGIT6);
  }

  private static String NSECONDS = "NSECONDS";

  public static void setNSeconds(int v) {
    setInt(NSECONDS, v);
  }

  public static int getNSeconds() {
    return getInt(NSECONDS, 0);
  }

  private static String NUMBER = "NUMBER";

  public static void setNumber(int v) {
    setInt(NUMBER, v);
  }

  public static int getNumber() {
    return getInt(NUMBER, 0);
  }

  private static String TARGET = "TARGET";

  public static void setTarget(int v) {
    setInt(TARGET, v);
  }

  public static int getTarget() {
    return getInt(TARGET, 0);
  }

  private static String NSCORE = "NSCORE";

  public static void setNScore(int v) {
    setInt(NSCORE, v);
  }

  public static int getNScore() {
    return getInt(NSCORE, 0);
  }

  private static String CALC = "CALC";

  public static void setCalc(String v) {
    setString(CALC, v);
  }

  public static String getCalc() {
    return getString(CALC, "");
  }

  private static String OP1 = "OP1";

  public static void setOp1(int v) {
    setInt(OP1, v);
  }

  public static int getOp1() {
    return getInt(OP1, 0);
  }

  private static String OP2 = "OP2";

  public static void setOp2(int v) {
    setInt(OP2, v);
  }

  public static int getOp2() {
    return getInt(OP2, 0);
  }

  private static String OP = "OP";

  public static void setOp(String v) {
    setString(OP, v);
  }

  public static String getOp() {
    return getString(OP, "");
  }

}